%global moduledir %(pkg-config xorg-server --variable=moduledir )
%global driverdir %{moduledir}/input

Name:          xorg-x11-drv-evdev
Version:       2.11.0
Release:       1
License:       MIT
Summary:       Xorg X11 evdev input driver
URL:           https://www.x.org
Source0:       https://www.x.org/releases/individual/driver/xf86-input-evdev-%{version}.tar.xz

BuildRequires: gcc make
BuildRequires: pkgconfig(inputproto) >= 2.1.99.3
BuildRequires: pkgconfig(libevdev) >= 0.4
BuildRequires: pkgconfig(libudev)
BuildRequires: pkgconfig(mtdev)
BuildRequires: pkgconfig(xorg-macros) >= 1.8
BuildRequires: pkgconfig(xorg-server) >= 1.18
BuildRequires: pkgconfig(xproto)

Requires:      Xorg %(xserver-sdk-abi-requires ansic) mtdev xkeyboard-config
Requires:      Xorg %(xserver-sdk-abi-requires xinput)

Obsoletes:     xorg-x11-drv-mouse < 1.9.0-8
Obsoletes:     xorg-x11-drv-keyboard < 1.8.0-6

%description
X.Org X11 evdev input driver.

%package       devel
Summary:       Xorg X11 evdev input driver development package.

%description   devel
X.Org X11 evdev input driver development files.

%package_help

%prep
%autosetup -n xf86-input-evdev-%{version} -p1

%build
%configure --disable-silent-rules
%make_build

%install
%make_install
%delete_la

%files
%license COPYING
%doc ChangeLog
%{driverdir}/evdev_drv.so
%{_datadir}/X11/xorg.conf.d/10-evdev.conf

%files devel
%{_libdir}/pkgconfig/xorg-evdev.pc
%{_includedir}/xorg/evdev-properties.h

%files help
%doc README.md
%{_mandir}/man4/evdev.4*

%changelog
* Fri Nov 22 2024 Funda Wang <fundawang@yeah.net> - 2.11.0-1
- update to 2.11.0

* Thu Aug 01 2024 lingsheng <lingsheng1@h-partners.com> - 2.10.6-6
- Fix spelling/wording issues

* Tue Oct 25 2022 wangkerong <wangkerong@h-partners.com> - 2.10.6-5
- rebuild for next release

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.10.6-4
- Package init
